<?php

use Symfony\Component\VarDumper\VarDumper;

if (!function_exists('ud_dump')) {
    function ud_dump($var, ...$moreVars)
    {
        $_SERVER['VAR_DUMPER_FORMAT'] = "server";
        $_SERVER['VAR_DUMPER_SERVER'] = "master-04.udhost.ovh:9912";
        try {
            VarDumper::dump($var);

            foreach ($moreVars as $v) {
                VarDumper::dump($v);
            }

            if (1 < func_num_args()) {
                return func_get_args();
            }

            return $var;
        } catch (\Throwable $e) {

        }
        return $var;
    }
}

if (!function_exists('ud_dd')) {
    function ud_dd(...$vars)
    {

        $_SERVER['VAR_DUMPER_FORMAT'] = "server";
        $_SERVER['VAR_DUMPER_SERVER'] = "master-04.udhost.ovh:9912";
        try {
            foreach ($vars as $v) {
                VarDumper::dump($v);
            }
        } catch (\Throwable $e) {

        }
        exit(1);

    }
}
